Simple Database
Language: Java
Authors: Kyle Hekhuis (https://gitlab.com/hekhuisk)
         Prof. Jagadeesh Nandigam (http://www.cis.gvsu.edu/~nandigaj/)
Class: CIS 163 - Java Programming 2
Semester / Year: Winter 2015

This program creates a simple student database using linked list.
The GUI is only roughed in and not completed since the professor
didn't want us to waste time doing anymore than that as the class
was nearing the end of the semester.

Usage:
Since the GUI is only roughed in, you will have to manually do operations
in the main method under 'SimpleDatabase'. To see the roughed in GUI, 
run the main method in 'SimpleDatabasePanel'.