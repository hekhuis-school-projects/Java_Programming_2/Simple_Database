package project4;

import java.text.*;

/**
 * Student class represents a student object with a name, gnumber, and gpa.
 * 
 * @version 1.0
 * 
 * @author Kyle Hekhuis and Nandigam
 */
public class Student implements Comparable<Student> {

	/** Name of the student */
    private String name;
    
    /** GNumber of the student */
    private String gnumber;
    
    /** GPA of the student */
    private double gpa;

    /**
     * Creates a Student object with the passed name, gnumber, and gpa.
     * 
     * @param name name of student
     * @param gnumber gnumber of student
     * @param gpa gpa of student
     */
    public Student(String name, String gnumber, double gpa) {
        this.name = name;
        this.gnumber = gnumber;
        this.gpa = gpa;
    }

    /**
     * Returns Student name.
     * 
     * @return student name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets Student name as passed string.
     * 
     * @param name string to set as name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns Student GNumber.
     * 
     * @return student gnumber
     */
    public String getGnumber() {
        return gnumber;
    }

    /**
     * Sets Student GNumber as passed string.
     * 
     * @param gnumber string to set as gnumber
     */
    public void setGnumber(String gnumber) {
        this.gnumber = gnumber;
    }

    /**
     * Returns Student gpa.
     * 
     * @return student gpa
     */
    public double getGpa() {
		return gpa;
	}

    /**
     * Sets Student gpa as passed double.
     * 
     * @param gpa double to set as gpa
     */
	public void setGpa(double gpa) {
		this.gpa = gpa;
	}

	/**
	 * Returns a string representation of a Student object.
	 * 
	 * @return a string representation of a Student object
	 */
	public String toString() {
		DecimalFormat df = new DecimalFormat("0.00");
        return name + ", " + gnumber + ", " + df.format(gpa);
    }

	/**
	 * Compares this object to the given object. The result is true
     * if and only if the argument is not null and is a Student 
     * object and the name, gnumber, and gpa of the specified Student is 
     * equal to the name, gnumber, and gpa of this Student object. It 
     * returns false otherwise.
	 * 
	 * @return true if the objects are equal, false otherwise
	 */
    public boolean equals(Object obj) {
        if(obj == null || !(obj instanceof Student)) {
        	return false;
        }
        
        Student s = (Student) obj;
        
        return this.name.equals(s.name) && 
        	   this.gnumber.equals(s.gnumber) && 
        	   this.gpa == s.gpa;
    }

    /**
     * Compares this object to the given Student object. The 
     * result is the value 0 if the argument is equal to this 
     * object; a value less than 0 if this object is less than 
     * the argument; and a value greater than 0 if this object 
     * is greater than the argument. The test for "equal" is 
     * based on the GNumber of the two Students.
     * 
     * @param ojb the Student object to compare this Student object against
     * 
     * @return the value 0 if the argument is equal to this object; a value 
     * less than 0 if this object is less than the argument; and a value 
     * greater than 0 if this object is greater than the argument.
     */
	@Override
	public int compareTo(Student obj) {
		return this.gnumber.compareTo(obj.gnumber);
	}
	
	/**
	 * Returns a hash code of this student.
	 * 
	 * @return hash code of this Student
	 */
	public int hashCode() {
		return name.hashCode() * gnumber.hashCode() * (int) gpa;
	}
    
}