package project4;

import java.util.Arrays;

/**
 * LinkedList class implements a doubly linked 
 * list of nodes of Student objects.
 * 
 * @version 1.0
 * 
 * @author Kyle Hekhuis
 */
public class LinkedList {

	/** First node of LinkedList */
	private Node head;
	
	/** Last node of LinkedList */
	private Node tail;
	
	/** Size of LinkedList */
	private int size;

	/**
	 * 
	 */
	public LinkedList() {
		head = null;
		tail = null;
		size = 0;
	}

	/** 
	 * Appends the given Student object to the end of the list.
	 * 
	 * @param data Student to add to list
	 * 
	 * @return true if Student successfully added to list,
	 * false otherwise.
	 */
	public boolean add(Student data) {
		if (data == null) {
			return false;
		}
		
		Node node = new Node(data);
		
		if (head == null && tail == null) { // list is empty
			head = node;
			tail = node;
			size = 1;
		} else {
			tail.setNext(node);
			node.setPrevious(tail);
			tail = node;
			size++;
		}
		return true;
	}

	/**
	 * Inserts the given Student at the specified position in the list.
	 * 
	 * @param index index of list to insert Student
	 * @param data Student to add to list
	 * 
	 * @return true if Student successfully added to list,
	 * false otherwise.
	 */
	public boolean add(int index, Student data) {
		if (index < 0 || index > size || data == null) {
			return false;
		}

		Node node = new Node(data);

		if (index == 0) {	// insert at the front
			node.setNext(head);
			head.setPrevious(node);
			head = node;
			if (tail == null) {
				tail = node;
			}
		} else if (index == size) {	// insert at the end
			tail.setNext(node);
			node.setPrevious(tail);
			tail = node;
		} else {
			Node current = head;	// insert in the middle
			for (int i = 0; i < index - 1; i++) {
				current = current.getNext();
			}
			node.setNext(current.getNext());
			current.getNext().setPrevious(node);;
			node.setPrevious(current);
			current.setNext(node);
		}
		
		size++;
		return true;
	}

	/**
	 * Removes the Student object at the specified 
	 * position in the list and returns it.
	 * 
	 * @param index index of Student object to remove from list
	 * 
	 * @return Student object that was removed
	 */
	public Student remove(int index) {
		if (index < 0 || index >= size) {
			throw new IndexOutOfBoundsException();
		}

		Node current = head;
		Node prev = head;

		// find the node to remove
		for (int i = 0; i < index; i++) {
			prev = current;
			current = current.getNext();
		}

		// remove the node from list
		if (current == head && current == tail) {
			head = null;
			tail = null;
		} else if (current == head) {
			head = head.getNext();
			head.setPrevious(null);
		} else if (current == tail) {
			prev.setPrevious(tail);
			tail = prev;
			tail.setNext(null);
		} else {
			prev.setPrevious(current.getPrevious());
			prev.setNext(current.getNext());
		}
		size--;
		
		// return the element removed from the list
		return current.getData();
	}

	/**
	 * Removes the first occurrence of the specified 
	 * Student from the list, if present.
	 * 
	 * @param data Student to remove from list
	 * 
	 * @return true if Student successfully removed 
	 * from the list, false otherwise.
	 */
	public boolean remove(Student data) {
		int index = indexOf(data);
		if (index >= 0) {
			remove(index);
			return true;
		}
		return false;
	}

	/**
	 * Returns the element at the specified position in the list.
	 * 
	 * @param index index of Student to get
	 * 
	 * @return student at specified position in the list
	 */
	public Student get(int index) {
		if (index < 0 || index >= size) {
			throw new IndexOutOfBoundsException();
		}
		
		Node temp = head;
		for (int i = 0; i < index; i++) {
			temp = temp.getNext();
		}
		return temp.getData();
	}

	/**
	 * Checks if the list contains no elements.
	 * 
	 * @return true if the list contains no elements, false otherwise
	 */
	public boolean isEmpty() {
		return size == 0;
	}

	/**
	 * Returns the number of elements in the list.
	 * 
	 * @return size of list
	 */
	public int size() {
		return size;
	}

	/**
	 * Removes all the elements from the list.
	 */
	public void clear() {
		size = 0;
		head = null;
		tail = null;
	}

	/**
	 * Checks if the list contains a specified element.
	 * 
	 * @param data Student object to see if contained in list
	 * 
	 * @return true if the list contains the specified element, false otherwise
	 */
	public boolean contains(Student data) {
		return indexOf(data) != -1;
	}

	/**
	 * Returns the index of the first occurrence of the specified element in
	 * this list, or -1 if this list does not contain the element.
	 * 
	 * @param data Student object to get index of
	 * 
	 * @return index of the passed Student object, or -1
	 * if it's not in the list
	 */
	public int indexOf(Student data) {
		if (data == null) {
			return -1;
		}
		
		Node temp = head;
		int i = 0;
		while (temp != null) {
			if (temp.getData().equals(data)) {
				return i;
			}
			temp = temp.getNext();
			i++;
		}

		return -1;
	}
	
	/**
	 * Returns a string representation of the elements in this list.
	 * 
	 * @return a string representation of the elements in this list
	 */
	public String toString() {
		String str = "";
		Node temp = head;
		while (temp != null) {
			str += temp + "\n";
			temp = temp.getNext();
		}
		return str;
	}
	
	/**
	 * Reverses the nodes in the list.
	 */
	public void reverse() {
		Node temp = head;
		Node prev = null;
		Node next = null;
		while (temp != null) {
			next = temp.getNext();
			temp.setNext(prev);
			prev = temp;
			temp = next;
		}
		temp = head;
		head = tail;
		tail = temp;
	}
	
	/**
	 * Sorts the Students in the list on GNumber field.
	 */
	public void sort() {
		// Make into an array in order to sort it
		Student[] temp = this.toArray();
		
		Arrays.sort(temp);
		
		// Clear list and add the sorted elements
		this.clear();
		for(int i = 0; i < temp.length; i++) {
			this.add(temp[i]);
		}
	}
	
	/**
	 * Takes the elements of the list and puts them into
	 * an array.
	 * 
	 * @return an array containing the elements of the list
	 */
	public Student[] toArray() {
		Student[] temp = new Student[this.size()];
		
		for(int i = 0; i < this.size(); i++) {
			temp[i] = this.get(i);
		}
		
		return temp;
	}
	
	/**
	 * Returns a cloned copy of this LinkedList.
	 * 
	 * @return cloned copy of this LinkedList
	 */
	public LinkedList clone() {
		LinkedList cloneList = new LinkedList();
		for(int i = 0; i < size; i++) {
			Student s = this.get(i);
			cloneList.add(new Student(s.getName(), s.getGnumber(), s.getGpa()));
		}
		return cloneList;
	}
}