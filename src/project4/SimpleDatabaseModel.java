package project4;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Stack;

/**
 * SimpleDatabaseModel class is a database that stores
 * information on a LinkedList of Students. Contains methods
 * for manipulating the database.
 * 
 * @version 1.0
 * 
 * @author Kyle Hekhuis
 */
public class SimpleDatabaseModel implements ISimpleDatabase {
	
	/** List of students in the database */
	private LinkedList list;
	
	/** Stack of saved LinkedList to get previous states of SimpleDatabaseModel */
	private Stack<LinkedList> undoList = new Stack<LinkedList>();
	
	/**
	 * Creates a new SimpleDatabaseModel object with
	 * an empty list.
	 */
	public SimpleDatabaseModel() {
		list = new LinkedList();
	}

	/**
	 * Inserts a Student object into the database.
	 * 
	 * @param student Student object to insert into the database
	 */
	public void insert(Student student) {
		if(student != null) { // Check if student is null
			undoList.push(list.clone()); // Add cloned list to undoList
			list.add(student);
		}
	}

	/**
	 * Deletes a Student object from the database.
	 * 
	 * @param gNumber gNumber of Student object to delete
	 * in database
	 * 
	 * @return true if Student object successfully deleted
	 * from database, false otherwise
	 */
	public boolean delete(String gNumber) {
		undoList.push(list.clone()); // Add cloned list to undoList
		return list.remove(find(gNumber));
	}

	/**
	 * Returns a string representation of SimpleDatabaseModel.
	 * 
	 * @return a string representation of SimpleDatabaseModel
	 */
	public String toString() {
		return list.toString();
	}
	
	/**
	 * Finds a specified Student object in the database.
	 * 
	 * @param gNumber gNumber of Student object to find
	 * in database
	 * 
	 * @return student with the specified gNumber
	 */
	public Student find(String gNumber) {
		for(int i = 0; i < list.size(); i++) {
			if(list.get(i).getGnumber().equals(gNumber)) {
				return list.get(i);
			}
		}
		return null;
	}
	
	/**
	 * Removes all elements from the database.
	 */
	public void clearDB() {
		undoList.push(list.clone()); // Add cloned list to undoList
		list.clear();
	}

	/**
	 * Loads the database from the specified file.
	 * 
	 * @param filename name of file to load database from
	 * 
	 * @throws IOException if fails to load file
	 */
	public void loadDB(String filename) throws IOException {
		clearDB();
		
		// Create new file to scan with passed filename
		File file = new File(filename);
		
		// Scan file
		Scanner sc = new Scanner(file);
		
		// Gets a line of data at a time and passes to parseStudent
		while(sc.hasNextLine()) {
			Student temp = parseStudent(sc.nextLine());
			insert(temp);
			// Undo the addition to undoList from insert
			undoList.pop();
		}
		
		sc.close();
	}
	
	/**
	 * Parses a string of data to retrieve a name, gnumber, and gpa
	 * and uses them to create a new Student object that is then returned.
	 * 
	 * @param data string of data containing name, gnumber, and gpa
	 * 
	 * @return Student object created from parsed data string
	 */
	private Student parseStudent(String data) {
		int comma1 = data.indexOf(",");
        int comma2 = data.indexOf(",", comma1 + 1);
        
        String name = data.substring(0, comma1);
		String gnumber = data.substring(comma1 + 2, comma2);
		double gpa = Double.parseDouble(data.substring(comma2 + 2));
		
		Student student = new Student(name, gnumber, gpa);
		return student;
	}

	/**
	 * Save the database to the specified file.
	 * 
	 * @param filename name of file to save database to
	 * 
	 * @throws IOException if fails to save file
	 */
	public void saveDB(String filename) throws IOException {
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(filename)));
		String temp = toString().replaceAll("\n", System.lineSeparator());
		out.print(temp);
		out.close();
		// Clear undoList so nothing can be undone once saved
		undoList.clear();
	}

	/**
	 * Reverses the database.
	 */
	public void reverse() {
		undoList.push(list.clone()); // Add cloned list to undoList
		list.reverse();
	}
	
	/**
	 * Removes duplicates from the database.
	 */
	public void removeDuplicates() {
		undoList.push(list.clone()); // Add cloned list to undoList
		
		Student[] students = list.toArray();
		LinkedHashSet<Student> hash = new LinkedHashSet<Student>();
		list.clear();
		for(int i = 0; i < students.length; i++) {
			hash.add(students[i]);
		}
		for(Student s: hash) {
			insert(s);
			// Undo the addition to undoList from insert
			undoList.pop();
		}
	}
	
	/**
	 * Sorts the database based off Student objects gNumber field.
	 */
	public void sort() {
		undoList.push(list.clone()); // Add cloned list to undoList
		list.sort();
	}

	/**
	 * Undoes the previous action done (insert, delete, clearDB, 
	 * reverse, removeDuplicates, sort, and loadDB). Can perform
	 * for as many actions done. Saving the database sets the
	 * amount of actions done to 0.
	 */
	public void undo() {
		list = undoList.pop();
	}
}