package project4;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class SimpleDatabase {

		public static void main(String[] args) {
			ISimpleDatabase model = new SimpleDatabaseModel();
			Student s1 = new Student("John Doe", "G123456789", 3.5);
			Student s2 = new Student("Paul Graham", "G123456987", 2.75);
			Student s3 = new Student("Mary Joe", "G331456987", 3.25);
			Student s4 = new Student("Martin Fowler", "G654789321", 3.65);
			model.insert(s1);
			model.insert(s2);
			model.insert(s3);
			model.insert(s4);
			try {
				model.saveDB("test5.txt");
			} catch (IOException ex) {}
			
			System.out.println(model.toString());
			
			model.clearDB();
			
			System.out.println(model.toString());
			System.out.println("2");
			
			File file = new File("test5.txt");
			
			String data = "";
			
			Scanner sc;
			try {
				sc = new Scanner(file);
				data = sc.nextLine();
				System.out.println(data);
				String data2 = sc.nextLine();
				System.out.println(data2);
				sc.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			int comma1 = data.indexOf(",");
	        int comma2 = data.indexOf(",", comma1 + 1);
	        
			String name = data.substring(0, comma1);
			String gnumber = data.substring(comma1 + 2, comma2);
			double gpa = Double.parseDouble(data.substring(comma2 + 2));
			
			System.out.println(name);
			System.out.println(gnumber);
			System.out.println(gpa);
			
			
			Student student = new Student(name, gnumber, gpa);
			System.out.println(student.toString());
			
		}
}
