package project4;

import java.io.*;

/************************************************************
 *		DO NOT MAKE CHANGES TO THIS FILE					*
 ************************************************************/
 
interface ISimpleDatabase {

   // The following methods are implemented in Steps 1 - 5

   /* insert student into the DB as a last record */
   void insert(Student student);

   /* delete the first student with the given gnumber */
   boolean delete(String gnumber);

   /* returns a string of the entire DB */
   String toString();

   /* finds and return the first student with the given gnumber */
   Student find(String gnumber);

   /* remove all students from the database */
   void clearDB();

   /* load the database from the specified file */
   void loadDB(String filename) throws IOException;

   /* save the database to the specified file */
   void saveDB(String filename) throws IOException;

   // The following methods are implemented in Steps 6 - 9

   /* Step 6: reverses the database */
   void reverse();

   /* Step 7: removes duplicates from the database */
   void removeDuplicates();

   /* Step 8: sort records (on gnumber field) in the database */
   void sort();

   /* Step 9: undo the previous command */
   void undo();  

}