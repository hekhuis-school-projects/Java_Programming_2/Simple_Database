package project4;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class SimpleDatabasePanel {

	private JFrame frame;
	private JPanel panel;
	private JMenu fileMenu;
	private JMenuBar menuBar;
	private JMenuItem exitMenuItem;
	private JButton insert,
					delete,
					find,
					reverse,
					duplicate,
					display,
					load,
					save;
	private JTextField nameTxt,
					   gnumberTxt,
					   gpaTxt;
	private JLabel nameLbl,
				   gnumberLbl,
				   gpaLbl;
	private ButtonListener listener;
	private JTextArea textArea;
	
	public SimpleDatabasePanel() {
		setupFrame();
	}
	
	/**
	 * Setup the menu for the frame.
	 */
	private void setupMenu() {
		menuBar = new JMenuBar();
		
		fileMenu = new JMenu("File");
		exitMenuItem = new JMenuItem("Exit");
		
		fileMenu.add(exitMenuItem);
		
		menuBar.add(fileMenu);
	}
	
	private JPanel setupPanel1() {
		JPanel p1 = new JPanel();
		p1.setLayout(new GridLayout(0, 4, 7, 0));
		
		p1.add(setupLabels());
		p1.add(setupTextFields());
		p1.add(setupButtons1());
		p1.add(setupButtons2());
		
		return p1;
	}
	
	private JPanel setupLabels() {
		JPanel p = new JPanel();
		p.setLayout(new GridLayout(4, 0, 0, 7));
		
		// Setup labels
		nameLbl = new JLabel("Name:");
		gnumberLbl = new JLabel("G Number:");
		gpaLbl = new JLabel("GPA:");
		
		// Add labels
		p.add(nameLbl);
		p.add(gnumberLbl);
		p.add(gpaLbl);
		
		return p;
	}
	
	private JPanel setupTextFields() {
		JPanel p = new JPanel();
		p.setLayout(new GridLayout(4, 0, 0, 7));
		
		// Setup text fields
		nameTxt = new JTextField(14);
		gnumberTxt = new JTextField(14);
		gpaTxt = new JTextField(14);
		
		// Add text fields
		p.add(nameTxt);
		p.add(gnumberTxt);
		p.add(gpaTxt);
		
		return p;
	}
	
	private JPanel setupButtons1() {
		JPanel p = new JPanel();
		p.setLayout(new GridLayout(4, 0, 0, 7));
		
		// Setup buttons
		insert = new JButton("Insert");
		find = new JButton("Find");
		duplicate = new JButton("Duplicate");
		load = new JButton("Load");
		
		// Add buttons
		p.add(insert);
		p.add(find);
		p.add(duplicate);
		p.add(load);
		
		return p;
	}
	
	private JPanel setupButtons2() {
		JPanel p = new JPanel();
		p.setLayout(new GridLayout(4, 0, 0, 7));
		
		// Setup buttons
		delete = new JButton("Delete");
		reverse = new JButton("Reverse");
		display = new JButton("Display");
		save = new JButton("Save");
		
		// Add buttons
		p.add(delete);
		p.add(reverse);
		p.add(display);
		p.add(save);
		
		return p;
	}
	
	private JPanel setupPanel2() {
		JPanel p2 = new JPanel();
		
		textArea = new JTextArea(10, 60);
		textArea.setEditable(false);
		JScrollPane scrollPane = new JScrollPane(textArea);
		
		p2.add(scrollPane);
		
		return p2;
	}
	
	private void setupFrame() {
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel = new JPanel(new BorderLayout());
		
		setupMenu();
		frame.setJMenuBar(menuBar);
		panel.add(setupPanel1(), BorderLayout.NORTH);
		panel.add(setupPanel2(), BorderLayout.SOUTH);
		frame.add(panel);
		
		listener = new ButtonListener();
		addActionListeners();
		
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setResizable(true);
		frame.setVisible(true);
	}
	
	/**
	 * Add actionListeners to all buttons.
	 */
	private void addActionListeners() {
		exitMenuItem.addActionListener(listener);
		insert.addActionListener(listener);
		delete.addActionListener(listener);
		find.addActionListener(listener);
		reverse.addActionListener(listener);
		duplicate.addActionListener(listener);
		display.addActionListener(listener);
		load.addActionListener(listener);
		save.addActionListener(listener);
	}
	
	private class ButtonListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			// Exits program
			if(e.getSource() == exitMenuItem) {
				System.exit(0);
			}
			// Insert
			if(e.getSource() == insert) {
				
			}
			// Delete
			if(e.getSource() == delete) {
				
			}
			// Find
			if(e.getSource() == find) {
				
			}
			// Reverse
			if(e.getSource() == reverse) {
				
			}
			// Duplicate
			if(e.getSource() == duplicate) {
				
			}
			// Display
			if(e.getSource() == display) {
				
			}
			// Load
			if(e.getSource() == load) {
				
			}
			// Save
			if(e.getSource() == save) {
				
			}
		} // End of actionPerformed
	} // End of ButtonListner
	
	public static void main(String args[]) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new SimpleDatabasePanel();
            }
        });
    }
}
