package project4;

/**
 * Node class represents a node containing a Student object.
 * 
 * @version 1.0
 * 
 * @author Kyle Hekhuis and Nandigam
 */
public class Node {

	/** Student object contained in the node */
    private Student data;
    
    /** The next node */
    private Node next;
    
    /** The previous node */
    private Node previous;

    /**
     * Creates a Node object with data, next,
     * and previous all set to null.
     */
    public Node() {
        data = null;
        next = null;
        previous = null;
    }

    /**
     * Creates a Node object with data set as the
     * the passed Student object, and next and
     * previous set to null.
     * 
     * @param data Student object to store in node
     */
    public Node(Student data) {
        this.data = data;
        this.next = null;
        this.previous = null;
    }

    /**
     * Returns the Node data.
     * 
     * @return node data
     */
    public Student getData() {
        return data;
    }

    /**
     * Sets the Node data
     * 
     * @param data Student to set as data
     */
    public void setData(Student data) {
        this.data = data;
    }

    /**
     * Returns the next Node to this Node.
     * 
     * @return next node of this node
     */
    public Node getNext() {
        return next;
    }

    /**
     * Sets the next Node to this Node.
     * 
     * @param next node to set as next node
     */
    public void setNext(Node next) {
        this.next = next;
    }
    
    /**
     * Returns the previous Node to this Node.
     * 
     * @return previous node of this node
     */
    public Node getPrevious() {
    	return previous;
    }
    
    /**
     * Sets the previous Node to this Node.
     * 
     * @param previous node to set as previous node
     */
    public void setPrevious(Node previous) {
    	this.previous = previous;
    }
    
    /**
     * Returns a string representation of a Node object.
	 * 
	 * @return a string representation of a Node object
     */
    public String toString() {
        return data.toString();
    }
}