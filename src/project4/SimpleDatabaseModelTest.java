package project4;

import java.io.*;
import static org.junit.Assert.*;

import org.junit.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.JVM)
public class SimpleDatabaseModelTest {

	@Test
	public void test_empty_database() {
		ISimpleDatabase model = new SimpleDatabaseModel();
		assertEquals("",model.toString().trim());
	}
	
	@Test
	public void test_insert_1() {
		ISimpleDatabase model = new SimpleDatabaseModel();
		Student s = new Student("John Doe", "G123456789", 3.5);
		model.insert(s);
		assertEquals(s.toString().trim(),model.toString().trim());
	}

	@Test
	public void test_insert_2() {
		ISimpleDatabase model = new SimpleDatabaseModel();
		Student s1 = new Student("John Doe", "G123456789", 3.5);
		Student s2 = new Student("Paul Graham", "G123456987", 2.75);
		Student s3 = new Student("Mary Joe", "G331456987", 3.25);
		Student s4 = new Student("Martin Fowler", "G654789321", 3.65);
		model.insert(s1);
		model.insert(s2);
		model.insert(s3);
		model.insert(s4);
		String expected = s1 + "\n" + s2 + "\n" + s3 + "\n" + s4;
		assertEquals(expected,model.toString().trim());
	}
	
	@Test
	public void test_insert_3() {
		ISimpleDatabase model = new SimpleDatabaseModel();
		Student s1 = new Student("John Doe", "G123456789", 3.5);
		model.insert(s1);
		model.insert(s1);
		model.insert(s1);
		model.insert(s1);
		String expected = s1 + "\n" + s1 + "\n" + s1 + "\n" + s1;
		assertEquals(expected,model.toString().trim());
	}
	
	@Test
	public void test_delete_1() {
		ISimpleDatabase model = new SimpleDatabaseModel();
		Student s1 = new Student("John Doe", "G123456789", 3.5);
		Student s2 = new Student("Paul Graham", "G123456987", 2.75);
		Student s3 = new Student("Mary Joe", "G331456987", 3.25);
		Student s4 = new Student("Martin Fowler", "G654789321", 3.65);
		model.insert(s1);
		model.insert(s2);
		model.insert(s3);
		model.insert(s4);
		assertEquals(true,model.delete("G123456987"));
		assertEquals(true,model.delete("G654789321"));
		String expected = s1 + "\n" + s3;
		assertEquals(expected,model.toString().trim());
	}
	
	@Test
	public void test_delete_2() {
		ISimpleDatabase model = new SimpleDatabaseModel();
		Student s1 = new Student("John Doe", "G123456789", 3.5);
		Student s2 = new Student("Paul Graham", "G123456987", 2.75);
		Student s3 = new Student("Mary Joe", "G331456987", 3.25);
		Student s4 = new Student("Martin Fowler", "G654789321", 3.65);
		model.insert(s1);
		model.insert(s2);
		model.insert(s3);
		model.insert(s4);
		assertEquals(true,model.delete("G123456789"));
		assertEquals(true,model.delete("G123456987"));
		assertEquals(true,model.delete("G331456987"));
		assertEquals(true,model.delete("G654789321"));
		assertEquals("",model.toString().trim());
	}
	
	@Test
	public void test_delete_3() {
		ISimpleDatabase model = new SimpleDatabaseModel();
		Student s1 = new Student("John Doe", "G123456789", 3.5);
		Student s2 = new Student("Paul Graham", "G123456987", 2.75);
		Student s3 = new Student("Mary Joe", "G331456987", 3.25);
		Student s4 = new Student("Martin Fowler", "G654789321", 3.65);
		Student s5 = new Student("Mary Joe", "G331456987", 3.25);
		model.insert(s1);
		model.insert(s2);
		model.insert(s3);
		model.insert(s4);
		model.insert(s5);
		assertEquals(true,model.delete("G331456987"));
		String expected = s1 + "\n" + s2 + "\n" + s4 + "\n" + s5;
		assertEquals(expected,model.toString().trim());
	}
	
	@Test
	public void test_delete_4() {
		ISimpleDatabase model = new SimpleDatabaseModel();
		Student s1 = new Student("John Doe", "G123456789", 3.5);
		Student s2 = new Student("Paul Graham", "G123456987", 2.75);
		Student s3 = new Student("Mary Joe", "G331456987", 3.25);
		Student s4 = new Student("Martin Fowler", "G654789321", 3.65);
		model.insert(s1);
		model.insert(s2);
		model.insert(s3);
		model.insert(s4);
		assertEquals(false,model.delete("G111222333"));
		String expected = s1 + "\n" + s2 + "\n" + s3 + "\n" + s4;
		assertEquals(expected,model.toString().trim());
	}
	
	@Test
	public void test_delete_5() {
		ISimpleDatabase model = new SimpleDatabaseModel();
		Student s1 = new Student("John Doe", "G123456789", 3.5);
		model.insert(s1);
		model.insert(s1);
		model.insert(s1);
		assertEquals(true,model.delete("G123456789"));
		String expected = s1 + "\n" + s1;
		assertEquals(expected,model.toString().trim());
	}
	
	@Test
	public void test_delete_6() {
		ISimpleDatabase model = new SimpleDatabaseModel();
		assertEquals(false,model.delete("G111222333"));
		assertEquals("",model.toString().trim());
	}
	
	@Test
	public void test_find_1() {
		ISimpleDatabase model = new SimpleDatabaseModel();
		Student s1 = new Student("John Doe", "G123456789", 3.5);
		Student s2 = new Student("Paul Graham", "G123456987", 2.75);
		Student s3 = new Student("Mary Joe", "G331456987", 3.25);
		Student s4 = new Student("Paul Graham", "G123456987", 2.75);
		Student s5 = new Student("Martin Fowler", "G654789321", 3.65);
		model.insert(s1);
		model.insert(s2);
		model.insert(s3);
		model.insert(s4);
		model.insert(s5);
		assertEquals(s2,model.find("G123456987"));
	}
	
	@Test
	public void test_find_2() {
		ISimpleDatabase model = new SimpleDatabaseModel();
		Student s1 = new Student("John Doe", "G123456789", 3.5);
		Student s2 = new Student("Paul Graham", "G123456987", 2.75);
		Student s3 = new Student("Mary Joe", "G331456987", 3.25);
		Student s4 = new Student("Martin Fowler", "G654789321", 3.65);
		model.insert(s1);
		model.insert(s2);
		model.insert(s3);
		model.insert(s4);
		assertEquals(null,model.find("G111222333"));
	}
	
	@Test
	public void test_find_3() {
		ISimpleDatabase model = new SimpleDatabaseModel();
		assertEquals(null,model.find("G111222333"));
	}
	
	@Test
	public void test_clearDB_1() {
		ISimpleDatabase model = new SimpleDatabaseModel();
		Student s1 = new Student("John Doe", "G123456789", 3.5);
		Student s2 = new Student("Paul Graham", "G123456987", 2.75);
		Student s3 = new Student("Mary Joe", "G331456987", 3.25);
		Student s4 = new Student("Martin Fowler", "G654789321", 3.65);
		model.insert(s1);
		model.insert(s2);
		model.insert(s3);
		model.insert(s4);
		model.clearDB();
		assertEquals("",model.toString().trim());
	}
	
	@Test
	public void test_clearDB_2() {
		ISimpleDatabase model = new SimpleDatabaseModel();
		model.clearDB();
		assertEquals("",model.toString().trim());
	}
	
	@Test
	public void test_saveload_1() {
		ISimpleDatabase model = new SimpleDatabaseModel();
		Student s1 = new Student("John Doe", "G123456789", 3.5);
		Student s2 = new Student("Paul Graham", "G123456987", 2.75);
		Student s3 = new Student("Mary Joe", "G331456987", 3.25);
		Student s4 = new Student("Martin Fowler", "G654789321", 3.65);
		model.insert(s1);
		model.insert(s2);
		model.insert(s3);
		model.insert(s4);
		try {
			model.saveDB("test.db");
			model.clearDB();
			assertEquals("",model.toString().trim());
			model.loadDB("test.db");
			String expected = s1 + "\n" + s2 + "\n" + s3 + "\n" + s4;
			assertEquals(expected,model.toString().trim());
		} catch (IOException ex) {}
	}
	
	@Test
	public void test_saveload_2() {
		ISimpleDatabase model = new SimpleDatabaseModel();
		Student s1 = new Student("John Doe", "G123456789", 3.5);
		model.insert(s1);
		try {
			model.saveDB("test.db");
			model.clearDB();
			assertEquals("",model.toString().trim());
			model.loadDB("test.db");
			assertEquals(s1.toString(),model.toString().trim());
		} catch (IOException ex) {}
	}
	
	@Test
	public void test_saveload_3() {
		ISimpleDatabase model = new SimpleDatabaseModel();
		try {
			model.saveDB("test.db");
			assertEquals("",model.toString().trim());
			model.loadDB("test.db");
			assertEquals("",model.toString().trim());
		} catch (IOException ex) {}
	}
	
	@Test
	public void test_reverse_1() {
		ISimpleDatabase model = new SimpleDatabaseModel();
		Student s1 = new Student("John Doe", "G123456789", 3.5);
		Student s2 = new Student("Paul Graham", "G123456987", 2.75);
		Student s3 = new Student("Mary Joe", "G331456987", 3.25);
		Student s4 = new Student("Martin Fowler", "G654789321", 3.65);
		model.insert(s1);
		model.insert(s2);
		model.insert(s3);
		model.insert(s4);
		String expected = s1 + "\n" + s2 + "\n" + s3 + "\n" + s4;
		assertEquals(expected,model.toString().trim());
		model.reverse();
		expected = s4 + "\n" + s3 + "\n" + s2 + "\n" + s1;
		assertEquals(expected,model.toString().trim());
	}
	
	@Test
	public void test_reverse_2() {
		ISimpleDatabase model = new SimpleDatabaseModel();
		model.reverse();
		assertEquals("",model.toString().trim());
	}
	
	@Test
	public void test_removeDuplicates_1() {
		ISimpleDatabase model = new SimpleDatabaseModel();
		Student s1 = new Student("John Doe", "G123456789", 3.5);
		Student s2 = new Student("Paul Graham", "G123456987", 2.75);
		Student s3 = new Student("Mary Joe", "G331456987", 3.25);
		Student s4 = new Student("Martin Fowler", "G654789321", 3.65);
		Student s5 = new Student("Paul Graham", "G123456987", 2.75);
		Student s6 = new Student("Mary Joe", "G331456987", 3.25);
		model.insert(s1);
		model.insert(s2);
		model.insert(s3);
		model.insert(s4);
		model.insert(s5);
		model.insert(s6);
		model.removeDuplicates();
		String expected = s1 + "\n" + s2 + "\n" + s3 + "\n" + s4;
		assertEquals(expected,model.toString().trim());
	}
	
	@Test
	public void test_removeDuplicates_2() {
		ISimpleDatabase model = new SimpleDatabaseModel();
		Student s1 = new Student("John Doe", "G123456789", 3.5);
		Student s2 = new Student("Paul Graham", "G123456987", 2.75);
		Student s3 = new Student("Mary Joe", "G331456987", 3.25);
		Student s4 = new Student("Martin Fowler", "G654789321", 3.65);
		model.insert(s1);
		model.insert(s2);
		model.insert(s3);
		model.insert(s4);
		model.removeDuplicates();
		String expected = s1 + "\n" + s2 + "\n" + s3 + "\n" + s4;
		assertEquals(expected,model.toString().trim());
	}
	
	@Test
	public void test_removeDuplicates_3() {
		ISimpleDatabase model = new SimpleDatabaseModel();
		Student s1 = new Student("John Doe", "G123456789", 3.5);
		model.insert(s1);
		model.insert(s1);
		model.insert(s1);
		model.insert(s1);
		String expected = s1 + "\n" + s1 + "\n" + s1 + "\n" + s1;
		assertEquals(expected,model.toString().trim());
		model.removeDuplicates();
		assertEquals(s1.toString(),model.toString().trim());
	}
	
	@Test
	public void test_removeDuplicates_4() {
		ISimpleDatabase model = new SimpleDatabaseModel();
		model.removeDuplicates();
		assertEquals("",model.toString().trim());
	}
	
	@Test
	public void test_sort_1() {
		ISimpleDatabase model = new SimpleDatabaseModel();
		Student s1 = new Student("John Doe", "G222222222", 3.5);
		Student s2 = new Student("Paul Graham", "G555555555", 2.75);
		Student s3 = new Student("Mary Joe", "G999999999", 3.25);
		Student s4 = new Student("Martin Fowler", "G111111111", 3.85);
		Student s5 = new Student("John Lewis", "G444444444", 3.15);
		Student s6 = new Student("Roger Pressman", "G333333333", 3.52);
		model.insert(s1);
		model.insert(s2);
		model.insert(s3);
		model.insert(s4);
		model.insert(s5);
		model.insert(s6);
		model.sort();
		String expected = s4 + "\n" + s1 + "\n" + s6 + "\n" + s5 + "\n" + s2 + "\n" + s3;
		assertEquals(expected,model.toString().trim());
	}
	
	@Test
	public void test_sort_2() {
		ISimpleDatabase model = new SimpleDatabaseModel();
		Student s1 = new Student("John Doe", "G222222222", 3.5);
		Student s2 = new Student("Paul Graham", "G555555555", 2.75);
		Student s3 = new Student("Mary Joe", "G999999999", 3.25);
		model.insert(s1);
		model.insert(s2);
		model.insert(s3);
		model.sort();
		String expected = s1 + "\n" + s2 + "\n" + s3;
		assertEquals(expected,model.toString().trim());
	}
	
	@Test
	public void test_sort_3() {
		ISimpleDatabase model = new SimpleDatabaseModel();
		Student s1 = new Student("John Doe", "G222222222", 3.5);
		Student s2 = new Student("Paul Graham", "G555555555", 2.75);
		Student s3 = new Student("Mary Joe", "G999999999", 3.25);
		model.insert(s3);
		model.insert(s2);
		model.insert(s1);
		model.sort();
		String expected = s1 + "\n" + s2 + "\n" + s3;
		assertEquals(expected,model.toString().trim());
	}
	
	@Test
	public void test_sort_4() {
		ISimpleDatabase model = new SimpleDatabaseModel();
		Student s1 = new Student("John Doe", "G222222222", 3.5);
		model.insert(s1);
		model.sort();
		assertEquals(s1.toString(),model.toString().trim());
	}
	
	@Test
	public void test_sort_5() {
		ISimpleDatabase model = new SimpleDatabaseModel();
		model.sort();
		assertEquals("",model.toString().trim());
	}
	
	@Test
	public void test_undo_1() {
		ISimpleDatabase model = new SimpleDatabaseModel();
		Student s1 = new Student("John Doe", "G123456789", 3.5);
		Student s2 = new Student("Paul Graham", "G123456987", 2.75);
		Student s3 = new Student("Mary Joe", "G331456987", 3.25);
		Student s4 = new Student("Martin Fowler", "G654789321", 3.65);
		model.insert(s1);
		model.insert(s2);
		model.insert(s3);
		model.insert(s4);
		model.undo();
		model.undo();
		String expected = s1 + "\n" + s2;
		assertEquals(expected,model.toString().trim());
	}
	
	@Test
	public void test_undo_2() {
		ISimpleDatabase model = new SimpleDatabaseModel();
		Student s1 = new Student("John Doe", "G123456789", 3.5);
		Student s2 = new Student("Paul Graham", "G123456987", 2.75);
		Student s3 = new Student("Mary Joe", "G331456987", 3.25);
		Student s4 = new Student("Martin Fowler", "G654789321", 3.65);
		model.insert(s1);
		model.insert(s2);
		model.insert(s3);
		model.insert(s4);
		model.undo();
		model.undo();
		model.undo();
		model.undo();
		assertEquals("",model.toString().trim());
	}
	
	@Test
	public void test_undo_3() {
		ISimpleDatabase model = new SimpleDatabaseModel();
		Student s1 = new Student("John Doe", "G123456789", 3.5);
		Student s2 = new Student("Paul Graham", "G123456987", 2.75);
		Student s3 = new Student("Mary Joe", "G331456987", 3.25);
		Student s4 = new Student("Martin Fowler", "G654789321", 3.65);
		model.insert(s1);
		model.insert(s2);
		model.insert(s3);
		model.insert(s4);
		model.delete("G123456987");
		model.delete("G654789321");
		model.undo();
		String expected = s1 + "\n" + s3 + "\n" + s4;
		assertEquals(expected,model.toString().trim());
	}
	
	@Test
	public void test_undo_4() {
		ISimpleDatabase model = new SimpleDatabaseModel();
		Student s1 = new Student("John Doe", "G123456789", 3.5);
		Student s2 = new Student("Paul Graham", "G123456987", 2.75);
		Student s3 = new Student("Mary Joe", "G331456987", 3.25);
		Student s4 = new Student("Martin Fowler", "G654789321", 3.65);
		model.insert(s1);
		model.insert(s2);
		model.insert(s3);
		model.insert(s4);
		model.reverse();
		model.undo();
		String expected = s1 + "\n" + s2 + "\n" + s3 + "\n" + s4;
		assertEquals(expected,model.toString().trim());
	}
	
	@Test
	public void test_undo_5() {
		ISimpleDatabase model = new SimpleDatabaseModel();
		Student s1 = new Student("John Doe", "G123456789", 3.5);
		Student s2 = new Student("Paul Graham", "G123456987", 2.75);
		Student s3 = new Student("Mary Joe", "G331456987", 3.25);
		Student s4 = new Student("Martin Fowler", "G654789321", 3.65);
		Student s5 = new Student("Paul Graham", "G123456987", 2.75);
		Student s6 = new Student("Mary Joe", "G331456987", 3.25);
		model.insert(s1);
		model.insert(s2);
		model.insert(s3);
		model.insert(s4);
		model.insert(s5);
		model.insert(s6);
		model.removeDuplicates();
		model.undo();
		String expected = s1 + "\n" + s2 + "\n" + s3 + "\n" + s4 + "\n" + s5 + "\n" + s6;
		assertEquals(expected,model.toString().trim());
	}
	
	@Test
	public void test_undo_6() {
		ISimpleDatabase model = new SimpleDatabaseModel();
		Student s1 = new Student("John Doe", "G123456789", 3.5);
		Student s2 = new Student("Paul Graham", "G123456987", 2.75);
		Student s3 = new Student("Mary Joe", "G331456987", 3.25);
		Student s4 = new Student("Martin Fowler", "G654789321", 3.65);
		Student s5 = new Student("Paul Graham", "G123456987", 2.75);
		Student s6 = new Student("Mary Joe", "G331456987", 3.25);
		model.insert(s1);
		model.insert(s2);
		model.insert(s3);
		model.insert(s4);
		model.insert(s5);
		model.insert(s6);
		model.clearDB();
		model.undo();
		String expected = s1 + "\n" + s2 + "\n" + s3 + "\n" + s4 + "\n" + s5 + "\n" + s6;
		assertEquals(expected,model.toString().trim());
	}
	
}